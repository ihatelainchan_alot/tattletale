# This generates an absurd amount of links. You'll probably want to kill this
# early on . . .

import requests
import bs4
import json

print("Start ...")

all_articles = []

domain = "http://spiderbites.nytimes.com"
session = requests.session()
session.proxies={'http':'socks5h://localhost:9050',
                'https':'socks5h://localhost:9050'}
s = session.get(domain)
soup = bs4.BeautifulSoup(s.content,'lxml')
div = soup.find('div',attrs={'id':'articles'})
toplinks = [a['href'] for a in div.find_all('a')]

for i,link in enumerate(toplinks):
    print("Toplink count: {}".format(i))
    try:
        midlevel = session.get(domain+'/'+link)
        midsoup = bs4.BeautifulSoup(midlevel.content,'lxml')
        contentbox = midsoup.find('div',attrs={'id':'mainContent'})
        parts = [a['href'] for a in contentbox.find_all('a')][1:]
        for j,p in enumerate(parts):
            print("Part count: {}".format(j))
            try:
                sublevel = session.get(domain+p)
                subsoup = bs4.BeautifulSoup(sublevel.content,'lxml')
                contentbox = subsoup.find('div',attrs={'id':'mainContent'})
                articles = [a['href'] for a in contentbox.find_all('a')][1:]
                all_articles += articles
                with open('NYTArticles-part.json','w') as outfile: # In case we have to kill it
                    json.dump(all_articles,outfile)
            except Exception as e:
                print('Inner loop exception...\n{}'.format(type(e)))
                pass # We really don't care about failing on a few links.
    except Exception as e:
        print('Outter loop exception...\n{}'.format(type(e)))
        pass # see above
print('Complete. Saving, exiting...')
with open('NYTArticles.json','w') as outfile:
    json.dump(all_articles,outfile)
