#! /usr/bin/env/ python3

#################################################
# tattletale is a fun script to share your favorite
# pictures with your friends at the New York Times!
#################################################

# HTML parsing tools and selenium will be required
import requests
import os
import bs4
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import random
from time import sleep
import argparse
import json
import pdb


class Setup:
    '''
    Class container for setting driver profile preferences,
    fetching proxy lists and methods necessary to use the
    proxy functinality of the script
    '''

    def __init__(self, **kwargs):
        self.proxy_list_url = 'http://us-proxy.org'
        self.proxies = []
        self.tor_ip = '127.0.0.1'
        self.tor_port = 9050
        with open('User Agents') as infile:
            self.agents = infile.read().split('\n')
        self.flags = {
            'proxy':False,
            'tor':False}
        self.flags.update(kwargs)
        self.profile = webdriver.FirefoxProfile('ffprofile.AdNauseum')

    def fetch_proxies(self):
        # request us-proxy web page and return a list of
        # IP and port numbers [(IP,port),]
        proxies = []
        r = requests.get(self.proxy_list_url)
        # Exit on failure if we can't get a list
        if r.status_code != 200:
            print('[!] PROXY REQUEST FAILED!')
            print('[!] EXITING!')
            quit()
        soup = bs4.BeautifulSoup(r.text,'lxml')
        table = soup.find('table',attrs={'id':'proxylisttable'})
        rows = table.find_all('tr')
        for row in rows[1:-2]: #only view rows with proxy data
            data = row.find_all('td')
            ip = data[0].text
            port = int(data[1].text)
            proxies.append((ip,port))
            self.proxies = proxies
        return None

    def set_user_agent(self):
        agent = random.choice(self.agents)
        self.profile.set_preference('general.useragent.override',agent)
        return None

    def set_http_proxy(self):
        # take a webdriver profile (Firefox) and a tuple (IP:PORT)
        # as arguments and update the profile
        proxy = self.validate_proxy()
        self.profile.set_preference('network.proxy.type',1)
        self.profile.set_preference('network.proxy.http',proxy[0])
        self.profile.set_preference('network.proxy.http_port',proxy[1])
        self.profile.update_preferences()
        return None

    def validate_proxy(self):
        # Verify that a given proxy can connect to Google and return 200
        print('Testing proxies')
        validated = False
        count = 0
        while not validated:
            count += 1
            proxy = random.choice(self.proxies)
            p = {'http':proxy[0]+':'+str(proxy[1])}
            print('\rConnection attempts: [{0}]'.format(count),end='',flush=True)
            testpage = requests.get('http://www.google.com',proxies=p)
            if testpage.status_code == 200:
                print('\nSuccess!')
                validated = True
        return proxy

    def set_tor_proxy(self):
        # Set preferences to connect through TOR
        self.profile.set_preference('network.proxy.type',1)
        self.profile.set_preference('network.proxy.socks',self.tor_ip)
        self.profile.set_preference('network.proxy.socks_port',self.tor_port)
        self.profile.update_preferences()
        return None

    def start(self):
        # Launch a webdriver with our preferences set.
        self.set_user_agent()
        if self.flags['proxy']:
            self.fetch_proxies()
            self.set_http_proxy()
        if self.flags['tor']:
            self.set_tor_proxy()
        driver = webdriver.Firefox(self.profile)
        return driver

    def refresh(self,driver):
        driver.close()
        self.set_user_agent()
        if self.flags['proxy']:
            self.set_http_proxy()
        if self.flags['tor']:
            self.set_tor_proxy()
        driver = webdriver.Firefox(self.profile)
        return driver

class Responder:
    '''
    This class holds the methods used for generating responses to the form fields,
    such as names, email addresses, etc.
    '''
    def __init__(self):
        self.picfolder = os.path.join(os.getcwd(),'Payload')
        self.pics = os.listdir(self.picfolder)
        with open('namelist','r') as infile:
            self.names = json.load(infile)
        with open('NYTArticles.json','r') as infile:
            self.articles = json.load(infile)
        with open('baneposts.json','r') as infile:
            self.baneposts = json.load(infile)

    def gen_name(self):
        name = random.choice(self.names)
        return name

    def gen_email(self, name):
        email = '.'.join(name.split())+"@gmail.com"
        return email

    def gen_link(self):
        link_url = random.choice(self.articles)
        return link_url

    def gen_note(self):
        note = random.choice(self.baneposts)
        return note

    def get_file(self):
        pic = random.choice(self.pics)
        pic_path = os.path.join(self.picfolder,pic)
        with open(pic_path,'a') as a:
            a.write('{}'.format(random.randint(0,100000000000)))
        return pic_path

class FormBot:
    """
    This is where all the logic lives that controls the interaction
    with the selenium instance and the NYT form.
    """

    def __init__(self,delay,driver_options):
        self.url = "https://www.nytimes.com/interactive/2017/admin/100000006090494.embedded.html"
        self.delay = delay
        self.driver_options = driver_options

    def launch(self):
        s = Setup(**self.driver_options) # Set UA and proxies, if applicable
        s.set_user_agent()
        if s.flags['proxy']:
            s.fetch_proxies()
            s.set_http_proxy()
        if s.flags['tor']:
            s.set_tor_proxy()
        print('Launching webdriver')
        return s.start() # return a webdriver object

    def fill_form(self,driver):
        driver.implicitly_wait(5)
        try:
            driver.get(self.url)
            responder = Responder()
            name = WebDriverWait(driver, 15).until(
                EC.presence_of_element_located((By.NAME, "name_identity"))
                )
            applicant_name = responder.gen_name()
            name.send_keys(applicant_name)
            email = driver.find_element_by_name("email_identity")
            email.send_keys(responder.gen_email(applicant_name))
            link = driver.find_element_by_name("relevant_link_text")
            link.send_keys(responder.gen_link())
            note = driver.find_element_by_name("leave_us_a_note_paragraph")
            note.send_keys(responder.gen_note())
            fileinput = driver.find_element_by_id("fileinput")
            fileinput.send_keys(responder.get_file())
            submit = WebDriverWait(driver, 15).until(
                EC.element_to_be_clickable((By.NAME, "submit"))
                )
            submit.click()
        except KeyboardInterrupt:
            sys.exit()
            pass
        except Exception as e:
            print("caught exception while handling form ... passing")
            print("{}".format(type(e)))
            pass # Just continue
        #sleepytime
        sleep(random.random()*self.delay)
        return None


class BanePost:
    '''
    This is a big class
    '''
    def __init__(self):
        self.files = ['Bane/CIA.txt','Bane/big_guy.txt','Bane/crash.txt']

    def post_rare_bane(self):
        filename = random.choice(self.files)
        with open(filename) as maymay:
            print(maymay.read())
        return None



# Main loop
if __name__ == "__main__":
    #Handle User supplied arguments and provide help information
    parser = argparse.ArgumentParser()
    parser.add_argument('-p','--proxy',help="Connect through random proxy", action="store_true")
    parser.add_argument('-t','--tor',help="Connect through tor (localhost:9050)",action="store_true")
    parser.add_argument('-b','--bane',help="Dr. Pavel, I'm CIA",action="store_true")
    parser.add_argument('-r','--reset',help="Maximum number of cycles before reset",
                        default=10)
    parser.add_argument('-d','--delay',help="Wait timer maximum delay (seconds)",
                        default=10)
    args = parser.parse_args()
    driver_options = {
                    'proxy':args.proxy,
                    'tor':args.tor}
    if args.bane:
        bp = BanePost()
        bp.post_rare_bane()
        quit()

    print('Press ctl+c to end program')
    print('Initializing webdriver . . .')
    bot = FormBot(args.delay,driver_options)
    driver = bot.launch()
    counter = 0
    while True:
        bot.fill_form(driver)
        counter += 1
        if counter >= (1-random.random()%0.5)*args.reset:
            driver.close()
            driver = bot.launch()
